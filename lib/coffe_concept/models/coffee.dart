import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_practice/helpers/image_helper.dart';

/// Coffe Names
/*
- 1. Caramel Macchiato
- 2. Caramel Cold Drink
- 3. Iced Coffe Mocha
- 4. Caramelized Pecan Latte
- 5. Toffee Nut Latte
- 6. Capuchino
- 7. Toffee Nut Iced Latte
- 8. Americano
- 9. Vietnamese-Style Iced Coffee
- 10. Black Tea Latte
- 11. Classic Irish Coffee
- 12. Toffee Nut Crunch Latte
*/

class Coffee {
  final int id;
  final String? name;
  final String? image;
  final double? price;

  Coffee(this.id, {@required this.name, @required this.image, @required this.price});
}

double _doubleInRange(Random source, num start, num end) => source.nextDouble() * (end - start) + start;

class CoffeeDatasource {
  static List<Coffee> createDatasource() {
    return [
      Coffee(1, name: 'Caramel Macchiato', image: ImagePathHelper.coffe1, price: _doubleInRange(Random(), 3, 7)),
      Coffee(2, name: 'Caramel Cold Drink', image: ImagePathHelper.coffe2, price: _doubleInRange(Random(), 3, 7)),
      Coffee(3, name: 'Iced Coffe Mocha', image: ImagePathHelper.coffe3, price: _doubleInRange(Random(), 3, 7)),
      Coffee(4, name: 'Caramelized Pecan Latte', image: ImagePathHelper.coffe4, price: _doubleInRange(Random(), 3, 7)),
      Coffee(5, name: 'Toffee Nut Latte', image: ImagePathHelper.coffe5, price: _doubleInRange(Random(), 3, 7)),
      Coffee(6, name: 'Capuchino', image: ImagePathHelper.coffe6, price: _doubleInRange(Random(), 3, 7)),
      Coffee(7, name: 'Toffee Nut Iced Latte', image: ImagePathHelper.coffe7, price: _doubleInRange(Random(), 3, 7)),
      Coffee(8, name: 'Americano', image: ImagePathHelper.coffe8, price: _doubleInRange(Random(), 3, 7)),
      Coffee(9, name: 'Vietnamese-Style Iced Coffee', image: ImagePathHelper.coffe9, price: _doubleInRange(Random(), 3, 7)),
      Coffee(10, name: 'Black Tea Latte', image: ImagePathHelper.coffe10, price: _doubleInRange(Random(), 3, 7)),
      Coffee(11, name: 'Classic Irish Coffee', image: ImagePathHelper.coffe11, price: _doubleInRange(Random(), 3, 7)),
      Coffee(12, name: 'Toffee Nut Crunch Latte', image: ImagePathHelper.coffe12, price: _doubleInRange(Random(), 3, 7)),
    ];
  }
}
