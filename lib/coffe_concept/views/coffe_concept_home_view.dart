import 'package:flutter/material.dart';
import 'package:flutter_practice/coffe_concept/models/coffee.dart';
import 'package:flutter_practice/coffe_concept/views/coffe_concept_list_view.dart';
import 'package:flutter_practice/helpers/image_helper.dart';
import 'package:flutter_practice/routes.dart';

class CoffeeConceptHomeView extends StatelessWidget {
  const CoffeeConceptHomeView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    final datasource = CoffeeDatasource.createDatasource();
    return Scaffold(
      body: GestureDetector(
        onVerticalDragUpdate: (details) {
          if ((details.primaryDelta ?? 0) < -20) {
            Navigator.of(context).push(
              PageRouteBuilder(
                  transitionDuration: Duration(milliseconds: 500),
                  pageBuilder: (context, animation, _) {
                    return FadeTransition(
                      opacity: animation,
                      child: CoffeeConceptListView(),
                    );
                  }),
            );
            //TODO: Implement named route
            //Navigator.of(context).pushNamed(Routes.coffee_concept_list);
          }
        },
        child: Stack(
          children: [
            SizedBox.expand(
              child: DecoratedBox(
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    colors: [Colors.brown, Colors.white],
                  ),
                ),
              ),
            ),
            Positioned(
              left: 0,
              right: 0,
              top: size.height * 0.15,
              height: size.height * 0.4,
              child: Hero(
                tag: datasource[6].id.toString(),
                child: Image.asset(
                  datasource[6].image ?? '',
                ),
              ),
            ),
            Positioned(
              left: 0,
              right: 0,
              bottom: 0,
              height: size.height * 0.7,
              child: Hero(
                tag: datasource[7].id.toString(),
                child: Image.asset(
                  datasource[7].image ?? '',
                  fit: BoxFit.cover,
                ),
              ),
            ),
            Positioned(
              left: 0,
              right: 0,
              bottom: -size.height * 0.8,
              height: size.height,
              child: Hero(
                tag: datasource[8].id.toString(),
                child: Image.asset(
                  datasource[8].image ?? '',
                  fit: BoxFit.cover,
                ),
              ),
            ),
            Positioned(
              left: 0,
              right: 0,
              bottom: size.height * 0.25,
              height: 140,
              child: Image.asset(ImagePathHelper.coffeLogo),
            ),
          ],
        ),
      ),
    );
  }
}
