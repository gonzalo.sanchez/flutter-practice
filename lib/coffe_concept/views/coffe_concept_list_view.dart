/// Based on
/// https://www.youtube.com/watch?v=bYqeGigTsBw&t=2105s
///
///
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_practice/coffe_concept/models/coffee.dart';
import 'package:flutter_practice/routes.dart';

class CoffeeConceptListView extends StatefulWidget {
  const CoffeeConceptListView({Key? key}) : super(key: key);

  @override
  _CoffeeConceptListViewState createState() => _CoffeeConceptListViewState();
}

class _CoffeeConceptListViewState extends State<CoffeeConceptListView> {
  List<Coffee> _datasource = [];
  double _currentPage = 0;

  final _mainPageController = PageController(viewportFraction: 0.35); // Wanna see three items at the same time
  final _titlePageController = PageController();

  final _animationsDuration = Duration(milliseconds: 300);

  @override
  void initState() {
    super.initState();
    _datasource = CoffeeDatasource.createDatasource();
    _mainPageController.addListener(_mainScrollListener);
  }

  @override
  void dispose() {
    _mainPageController.removeListener(_mainScrollListener);
    _mainPageController.dispose();
    _titlePageController.dispose();
    super.dispose();
  }

  _mainScrollListener() {
    setState(() {
      _currentPage = _mainPageController.page ?? 0;
    });
  }

  Widget _bottomShadow(Size size) {
    return Positioned(
      left: 20,
      right: 20,
      bottom: -size.height * 0.22,
      height: size.height * 0.3,
      child: DecoratedBox(
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          boxShadow: [
            BoxShadow(
              color: Colors.brown,
              blurRadius: 90,
              offset: Offset.zero,
              spreadRadius: 45,
            )
          ],
        ),
      ),
    );
  }

  Widget _header(Size size) {
    /// Note the difference:
    /// Use _datasource[_currentPage.toInt()] for price
    /// Use _datasource[index.toInt()] for title (PageView.builder)

    Widget title(Size size) {
      return Expanded(
        child: PageView.builder(
            physics: NeverScrollableScrollPhysics(),
            controller: _titlePageController,
            itemCount: _datasource.length,
            itemBuilder: (context, index) {
              final double opacity = (1 - (index - _currentPage)).abs().clamp(0, 1);
              return Opacity(
                opacity: opacity,
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: size.width * 0.2),
                  child: Text(
                    _datasource[index.toInt()].name ?? '',
                    textAlign: TextAlign.center,
                    maxLines: 2,
                    style: TextStyle(fontSize: 25, fontWeight: FontWeight.w800),
                  ),
                ),
              );
            }),
      );
    }

    Widget price() {
      final coffeeItem = _datasource[_currentPage.toInt()];
      return AnimatedSwitcher(
        duration: _animationsDuration,
        transitionBuilder: (Widget child, Animation<double> animation) {
          return ScaleTransition(child: child, scale: animation);
        },
        child: Text(
          // To update only on page changes, not every scroll point: needs to set key to distinguish every Text widget
          '\$${(coffeeItem.price ?? 0).toStringAsFixed(2)}',
          style: TextStyle(fontSize: 25),
          key: ValueKey<int>(coffeeItem.id),
        ),
      );
    }

    return Positioned(
      top: 0,
      left: 0,
      right: 0,
      height: 100,
      child: Column(
        children: [
          title(size),
          price(),
        ],
      ),
    );
  }

  Widget _list(Size size) {
    double calculateScale(double fixedIndex) {
      /// Scale example for the first 3 visible items
      /// 2 -> 0.2
      /// 1 -> 0.6
      /// 0 -> 1.0
      /// Desired values:
      /// 2a = b = 0.2   x = 2
      ///  a + b = 0.6   x = 1
      ///     b  = 1     x = 0
      ///
      return -0.4 * fixedIndex + 1;
    }

    /// Doing the _datasource.length + 1 trick
    /// with: if (index == 0) return SizedBox.shrink();
    /// I make sure I only show the first item at the bottom when the view appears
    /// I get the proper index for the datasource by: [index - 1]

    return Transform.scale(
      scale: 1.6,
      alignment: Alignment.bottomCenter,
      child: PageView.builder(
          controller: _mainPageController,
          scrollDirection: Axis.vertical,
          itemCount: _datasource.length + 1,
          onPageChanged: (newPage) {
            // Update title page
            if (newPage < _datasource.length) {
              _titlePageController.animateToPage(newPage, duration: _animationsDuration, curve: Curves.easeOut);
            }
          },
          itemBuilder: (context, index) {
            if (index == 0) return SizedBox.shrink();
            print('Index ${index - 1}');
            final coffeItem = _datasource[index - 1];
            final fixedIndex = _currentPage - index + 1;
            final itemScale = calculateScale(fixedIndex);
            final opacity = itemScale.clamp(0.0, 1.0);

            return Padding(
              padding: const EdgeInsets.only(bottom: 40),
              child: Transform(
                alignment: Alignment.bottomCenter,
                transform: Matrix4.identity()
                  ..setEntry(3, 2, 0.001)
                  ..translate(
                    0.0,
                    size.height / 2.6 * (1 - itemScale).abs(),
                  )
                  ..scale(itemScale),
                child: Opacity(
                  opacity: opacity,
                  child: Image.asset(coffeItem.image ?? '', fit: BoxFit.fitHeight),
                ),
              ),
            );
          }),
    );
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Theme(
      data: ThemeData.light(),
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.transparent,
          elevation: 0,
          leading: BackButton(
            color: Colors.black,
            onPressed: () {
              Navigator.popUntil(context, ModalRoute.withName(Routes.screen_decider));
            },
          ),
        ),
        body: Stack(
          children: [
            _bottomShadow(size),
            _list(size),
            _header(size),
          ],
        ),
      ),
    );
  }
}
