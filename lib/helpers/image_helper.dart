class ImagePathHelper {
  static final coffeLogo = 'lib/assets/coffe_concept/logo.png';
  static final coffe1 = 'lib/assets/coffe_concept/coffe_1.png';
  static final coffe2 = 'lib/assets/coffe_concept/coffe_2.png';
  static final coffe3 = 'lib/assets/coffe_concept/coffe_3.png';
  static final coffe4 = 'lib/assets/coffe_concept/coffe_4.png';
  static final coffe5 = 'lib/assets/coffe_concept/coffe_5.png';
  static final coffe6 = 'lib/assets/coffe_concept/coffe_6.png';
  static final coffe7 = 'lib/assets/coffe_concept/coffe_7.png';
  static final coffe8 = 'lib/assets/coffe_concept/coffe_8.png';
  static final coffe9 = 'lib/assets/coffe_concept/coffe_9.png';
  static final coffe10 = 'lib/assets/coffe_concept/coffe_10.png';
  static final coffe11 = 'lib/assets/coffe_concept/coffe_11.png';
  static final coffe12 = 'lib/assets/coffe_concept/coffe_12.png';
}
