import 'package:flutter/material.dart';
import 'package:flutter_practice/coffe_concept/views/coffe_concept_home_view.dart';
import 'package:flutter_practice/coffe_concept/views/coffe_concept_list_view.dart';
import 'package:flutter_practice/routes.dart';
import 'package:flutter_practice/screen_decider.dart';

void main() {
  runApp(FlutterPractice());
}

class FlutterPractice extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Practice',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      initialRoute: Routes.screen_decider,
      routes: {
        Routes.screen_decider: (context) => ScreenDecider(),
        Routes.coffee_concept_home: (context) => CoffeeConceptHomeView(),
        Routes.coffee_concept_list: (context) => CoffeeConceptListView(),
      },
    );
  }
}
