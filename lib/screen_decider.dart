import 'package:flutter/material.dart';
import 'package:flutter_practice/routes.dart';

class ScreenDecider extends StatefulWidget {
  const ScreenDecider({Key? key}) : super(key: key);

  @override
  _ScreenDeciderState createState() => _ScreenDeciderState();
}

class _ScreenDeciderState extends State<ScreenDecider> {
  _showCoffeeConcept() {
    Navigator.pushNamed(context, Routes.coffee_concept_home);
  }

  Widget _getExampleButton(String title, VoidCallback action) {
    return Padding(
      padding: EdgeInsets.zero,
      child: Column(
        children: [
          ElevatedButton(
            style: ButtonStyle(backgroundColor: MaterialStateProperty.all(Colors.amber)),
            onPressed: action,
            child: Text(title),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final ThemeData theme = Theme.of(context);
    return Scaffold(
      appBar: AppBar(title: Text('Flutter Practice')),
      body: Stack(
        children: [
          Container(
            alignment: Alignment.center,
            height: 100,
            child: Text(
              'Excercises to try UI/UX approches',
              style: theme.textTheme.headline2?.copyWith(
                fontSize: 18,
                color: Colors.blueGrey,
                fontWeight: FontWeight.w500,
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 100),
            child: Container(
              alignment: Alignment.center,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  _getExampleButton('Coffe Concept', _showCoffeeConcept),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
